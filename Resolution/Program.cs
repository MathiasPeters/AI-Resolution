﻿using System;
using System.Collections.Generic;
using System.Linq;

// Disables "Does not override GetHashCode()" warning when overriding Equals()
#pragma warning disable 659

namespace Resolution
{
    internal class Program
    {
		/// <summary>
		/// The entry point for the program
		/// </summary>
        private static void Main()
		{
			RunProgram("input.txt", "output.txt");
			RunProgram("giant_in.txt", "giant_out.txt");
			RunProgram("loki_in.txt", "loki_out.txt");
			RunProgram("not_god_in.txt", "not_god_out.txt");
		}

		/// <summary>
		/// This method will just run a resolution with input from inputFile and then print the result to outputFile
		/// </summary>
		/// <param name="inputFile"></param>
		/// <param name="outputFile"></param>
	    private static void RunProgram(string inputFile, string outputFile)
	    {
			var clauses = Resolution(System.IO.File.ReadAllLines(inputFile));
			PrintClauses(clauses, outputFile);
			foreach (var clause in clauses)
			{
				Console.WriteLine(clause);
			}
		    if (!clauses.Last().IsEmptyClause())
		    {
			    Console.WriteLine("Invalid clause, could not deduce");
		    }
			Console.WriteLine("Size of final clause set: " + clauses.Count);
			Console.ReadLine();
		}

		/// <summary>
		/// The main resolution method
		/// The entire resolution process is either done by this method or delegated by this method
		/// </summary>
		/// <param name="clauseArray"></param>
        private static List<Clause> Resolution(IEnumerable<string> clauseArray)
        {
            var clauses = clauseArray.Select(clause => new Clause(clause)).ToList();

            var runLoop = true;
            while (runLoop)
            {
	            var clausesAscending = clauses.OrderBy(c => c.Literals.Count).ToList();
                var newClauses = new List<Clause>();
                for (var i = 0; i < clauses.Count; i++)
                {
                    for (var j = 0; j < clausesAscending.Count; j++)
                    {
	                    if (clauses[i].Equals(clausesAscending[j])) continue;

                        var resolvents = Resolve(clauses[i], clausesAscending[j]);
                        if (!resolvents.Any()) continue;

                        newClauses = GetUnion(newClauses, resolvents);
	                    if (!ContainsEmptySet(resolvents)) continue;

	                    clauses = GetUnion(clauses, newClauses);
	                    i = j = clauses.Count;
	                    runLoop = false;
                    }
                }
                if (runLoop)
                {
                    if (IsSubset(newClauses, clauses)) runLoop = false;
                }
                clauses = GetUnion(clauses, newClauses);
            }

			var finalClauseSet = RemoveUnnecessaryClauses(clauses);
	        return finalClauseSet;
        }

		/// <summary>
		/// When the final clause set is reached it may contain branches that are not necessary for the resolution
		/// This method will remove all unnecessary clauses from the final set
		/// </summary>
		/// <param name="clauses"></param>
		/// <returns></returns>
	    private static List<Clause> RemoveUnnecessaryClauses(List<Clause> clauses)
	    {
		    if (!clauses.Last().IsEmptyClause())
		    {
			    return clauses;
		    }

		    var result = new List<Clause>();

		    foreach (var t in clauses)
		    {
			    if (t.ResolvedFrom.Any() && !t.IsEmptyClause()) continue;
			    
			    result.Add(t);
			    result.AddRange(RecursivelyFindParents(t));
		    }

		    result = result.Distinct().OrderBy(c => c.ClauseNumber).ToList();
			UpdateClauseNumbers(result);
		    return result;
	    }

		/// <summary>
		/// When removing unnecessary clauses the RemoveUnnecessaryClauses-method will find a clause that is the empty set
		/// and then keep all parents to that clause. This function will find all relevant parents
		/// </summary>
		/// <param name="clause"></param>
		/// <returns></returns>
	    private static IEnumerable<Clause> RecursivelyFindParents(Clause clause)
	    {
		    var parents = new List<Clause>();

		    foreach (var parent in clause.ResolvedFrom)
		    {
				parents.Add(parent);
				parents.AddRange(RecursivelyFindParents(parent));			    
		    }

		    return parents;
	    }

		/// <summary>
		/// Returns true if the given list of clauses contains the empty set, false otherwise
		/// </summary>
		/// <param name="clauses"></param>
		/// <returns></returns>
        private static bool ContainsEmptySet(IEnumerable<Clause> clauses)
        {
            return clauses.Any(clause => clause.IsEmptyClause());
        }

		/// <summary>
		/// Performs the resolution of two clauses, in the given document on the ResolutionAlgorithm, this would
		/// correspond to "PL-RESOLVE"
		/// </summary>
		/// <param name="clause1"></param>
		/// <param name="clause2"></param>
		/// <returns></returns>
        private static List<Clause> Resolve(Clause clause1, Clause clause2)
        {
            var newClauses = new List<Clause>();
            var allLiterals = new List<Literal>(clause1.Literals);
            allLiterals.AddRange(clause2.Literals);

            var literalCombinations = MinusOneLiteralPair(allLiterals);
            while (literalCombinations.Any())
            {
	            var literalCombination = literalCombinations.First();
	            if (literalCombination.Count <= clause1.Literals.Count ||
	                literalCombination.Count <= clause2.Literals.Count)
	            {
					newClauses.Add(new Clause(literalCombinations.First(), clause1, clause2));
				}
                literalCombinations.AddRange(MinusOneLiteralPair(literalCombinations.First()));
                literalCombinations = literalCombinations.Skip(1).ToList();
            }

            return newClauses.OrderBy(c => c.Literals.Count).ToList();
        }

		/// <summary>
		/// This method will be given a list of all literals of two clauses and it will then systematically
		/// extract all possible combinations of literals given the fact that p and ~p cancels each other out 
		/// </summary>
		/// <param name="literals"></param>
		/// <returns></returns>
        private static List<List<Literal>> MinusOneLiteralPair(IReadOnlyList<Literal> literals)
        {
            var result = new List<List<Literal>>();

            for (var i = 0; i < literals.Count; i++)
            {
                for (var j = i+1; j < literals.Count; j++)
                {
	                if (literals[i].SameButNegated(literals[j]))
	                {
		                var toKeep = GetAllBut(literals, i, j);
		                result.Add(new List<Literal>(toKeep));
	                }
                }
            }

            return result;
        }

		/// <summary>
		/// Given a list of literals and two indicies, will return a new list of all items in the given list except
		/// those at the indicies represented by x and y
		/// </summary>
		/// <param name="literals"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
	    private static IEnumerable<Literal> GetAllBut(IEnumerable<Literal> literals, int x, int y)
	    {
		    return literals.Where((t, i) => i != x && i != y).ToList();
	    }

        /// <summary>
        /// Checks if set1 is subset of set2
        /// </summary>
        /// <param name="set1"></param>
        /// <param name="set2"></param>
        /// <returns></returns>
        private static bool IsSubset(IEnumerable<Clause> set1, ICollection<Clause> set2)
        {
            return set1.All(set2.Contains);
        }

		/// <summary>
		/// Gievn two lists, returns the union of those two lists
		/// </summary>
		/// <param name="set1"></param>
		/// <param name="set2"></param>
		/// <returns></returns>
        private static List<Clause> GetUnion(IEnumerable<Clause> set1, IEnumerable<Clause> set2)
        {
            var newSet = new List<Clause>(set1);
            foreach (var clause in set2)
            {
                if (!newSet.Contains(clause))
                {
                    newSet.Add(clause);
                }
            }
            UpdateClauseNumbers(newSet);
            return newSet;
        }

		/// <summary>
		/// A lot of unnecessary clauses might be created in this program, each getting an auto incrementing id
		/// After one or more clauses has been discarded, the ids have to be updated so that there are no "holes"
		/// in the ids
		/// </summary>
		/// <param name="clauses"></param>
        private static void UpdateClauseNumbers(IReadOnlyList<Clause> clauses)
        {
            for (var i = 0; i < clauses.Count; i++)
            {
                clauses[i].ClauseNumber = i + 1;
            }
            Clause.NextClauseNumber = clauses.Count + 1;
        }

	    /// <summary>
	    /// Prints all clauses in the given list in the following format:
	    /// Id. literals {ids of parent clauses}
	    /// And ending with a line saying how many clauses are in the set
	    /// </summary>
	    /// <param name="clauses"></param>
	    /// <param name="fileName"></param>
	    private static void PrintClauses(IReadOnlyCollection<Clause> clauses, string fileName)
        {
            var clausesAsText = clauses.Aggregate("", (current, clause) => current + (clause.ToString() + '\n'));
	        if (!clauses.Last().IsEmptyClause())
	        {
		        clausesAsText += "Invalid clause, could not deduce\n";
	        }
			clausesAsText += "Size of final clause set: " + clauses.Count;

            System.IO.File.WriteAllText(fileName, clausesAsText);
        }
    }

	/// <summary>
	/// A class representing a clause
	/// A clause consists of a set of literals and, for this program, also an id and a set of parent clauses from
	/// which the clause can be resolved
	/// </summary>
    internal class Clause
    {
        public int ClauseNumber;
        public List<Literal> Literals;
        public List<Clause> ResolvedFrom;

		/// <summary>
		/// When the literals are read from a file they are in a string format
		/// this constructor will create a new clause from one of those strings
		/// </summary>
		/// <param name="literals"></param>
        public Clause(string literals)
        {
            ClauseNumber = NextClauseNumber++;
            Literals = new List<Literal>();
            ResolvedFrom = new List<Clause>();

	        var splitLiterals = literals.Split(' ');
            foreach (var lit in splitLiterals)
            {
                Literals.Add(new Literal(lit));
            }
        }

		/// <summary>
		/// During resolution a new clause will have to be created from a set of literals and two parent clauses
		/// </summary>
		/// <param name="literals"></param>
		/// <param name="c1"></param>
		/// <param name="c2"></param>
        public Clause(IEnumerable<Literal> literals, Clause c1, Clause c2)
        {
            ClauseNumber = NextClauseNumber++;
            Literals = new List<Literal>(literals);
	        ResolvedFrom = new List<Clause> {c1, c2};
        }

		/// <summary>
		/// Returns true if the clause is the empty set, false otherwise
		/// </summary>
		/// <returns></returns>
        public bool IsEmptyClause()
        {
            return Literals.Count == 0;
        }

		/// <summary>
		/// Override tostring to print the clause in the format:
		/// id. literals {ids of parents}
		/// </summary>
		/// <returns></returns>
        public override string ToString()
        {
            var outputString = ClauseNumber + ". ";
            if (Literals.Count == 0)
            {
                outputString += "False";
            }
            else
            {
                for (var i = 0; i < Literals.Count; i++)
                {
                    if (i != 0) outputString += " ";
                    outputString += Literals[i];
                }
            }
            outputString += " {";
            for (var i = 0; i < ResolvedFrom.Count; i++)
            {
                if (i != 0) outputString += ", ";
                outputString += ResolvedFrom[i].ClauseNumber;
            }
            outputString += "}";
            return outputString;
        }

		/// <summary>
		/// Override Equals so that the program can recognize two clauses as equal even if they are not in
		/// the same location in memory. Equality should rather be derived from the literals and the parents
		/// from which the clause was resolved
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        public override bool Equals(object obj)
        {
            try
            {
                var clause = (Clause)obj;
	            return clause != null &&
					   clause.Literals.Count == Literals.Count &&
	                   clause.Literals.All(c => Literals.Contains(c));
            }
            catch
            {
                return false;
            }
        }

		// This static variable will keep track of the next available Id
		// Every time a new clause is constructed this variable is incremented
        public static int NextClauseNumber = 1;
    }

	/// <summary>
	/// A class to represent a literal
	/// A literal can be negated or not negated and it also has a value, or name
	/// </summary>
    internal class Literal
    {
        public bool Negated;
        public string Value;

		/// <summary>
		/// This constructor reads in a literal in string form and creates a new literal with the same information
		/// </summary>
		/// <param name="literal"></param>
        public Literal(string literal)
        {
            var numNegations = 0;
            for (var i = 0; i < literal.Length; i++)
            {
                if (literal[i] == '~')
                {
                    numNegations++;
                }
                else
                {
                    Value = literal.Substring(i);
                    break;
                }
            }

            Negated = numNegations % 2 != 0;
        }

		/// <summary>
		/// Returns true if the given literal has the same value but the opposite negation as this literal
		/// </summary>
		/// <param name="l"></param>
		/// <returns></returns>
        public bool SameButNegated(Literal l)
        {
            return l.Negated != Negated && l.Value == Value;
        }

		/// <summary>
		/// Override Equals so that two literals can be identified as equal even if they are not
		/// in the same location in memory. Equally is derived from Value and its negation
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        public override bool Equals(object obj)
        {
            try
            {
                var literal = (Literal)obj;
                return literal != null && (Negated == literal.Negated && Value == literal.Value);
            }
            catch
            {
                return false;
            }
        }

		/// <summary>
		/// Override tostring so that a literal can be correctly concatenated to a string
		/// </summary>
		/// <returns></returns>
        public override string ToString()
        {
            var outputString = "";
            if (Negated) outputString += "~";
            outputString += Value;
            return outputString;
        }
    }
}
