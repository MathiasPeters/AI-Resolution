# Logic resolution program

This program was created for a university assignment and deals with simplified logic resolution.  
The program reads from a file where each line is a logic statement. 
A statement can be:
* A variable (p)
* A negated variable (~p)
* An or-statement (p q)

The program will parse the statements and look for a contradiction.  
The simplest example of a contradiction would be:
```
p
~p
```

This program can therefore be used to solve logic clauses by specifying a contradiction as the last line (see input files in Examples folder)